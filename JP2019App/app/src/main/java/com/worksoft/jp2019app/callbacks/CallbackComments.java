package com.worksoft.jp2019app.callbacks;

import com.worksoft.jp2019app.models.Comments;

import java.util.ArrayList;
import java.util.List;

public class CallbackComments {

    public String status = "";
    public int count = -1;
    public List<Comments> comments = new ArrayList<>();

}
