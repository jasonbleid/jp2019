package com.worksoft.jp2019app.callbacks;

import com.worksoft.jp2019app.models.Category;

import java.util.ArrayList;
import java.util.List;

public class CallbackCategories {
    public String status = "";
    public int count = -1;
    public List<Category> categories = new ArrayList<>();
}
